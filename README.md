Black night wide theme for https://habr.com

![screenshot](/screenshot.png)

<div align='center'>
    <a href='https://gitlab.com/vitaly-zdanevich-styles/habr/-/raw/master/habr-black.user.css' alt='Install with Stylus'>
        <img src='https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat' />
    </a>
</div>

For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

Mirroring https://userstyles.world/style/14797/habr-com-black